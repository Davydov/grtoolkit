/*
 * Google Reader Toolkit: Utilities for use with Google Reader.
 * An Extension for the Firefox (and Mozilla?) Browser.
 *
 * Release _VERSION_
 * _DATE_
 *
 * http://www.myths.ru/grtoolkit
 *
 * Copyright 2006, Yasha Davidov
 * Copyright 2004-2006, Chad Everett
 * Licensed under the Open Software License version 3.0
 *
 * The program is licensed under the Open Software License version 3.0
 * http://www.opensource.org/licenses/osl-3.0.php
 *
 * If you find the software useful or even like it, then a simple 'thank you'
 * is always appreciated.  A reference back to me is even nicer.  If you find
 * a way to make money from the software, do what you feel is right.
 */
