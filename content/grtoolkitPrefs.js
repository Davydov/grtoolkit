\include{comment.js}

// Initiate a new preference instance.
var grPref = Components.classes['@mozilla.org/preferences-service;1'].getService(Components.interfaces.nsIPrefBranch);

// Declare form variables.
var _elementIDs = [
  'grtoolkit.context.active',
  'grtoolkit.context.sep.a',
  'grtoolkit.context.sep.b',
  'grtoolkit.context.sep.b',
  'grtoolkit.link.background',
  'grtoolkit.link.option',
//  'grtoolkit.link.secure',
  'grtoolkit.notifier.active',
//  'grtoolkit.notifier.email',
  'grtoolkit.notifier.pause',
  'grtoolkit.notifier.pause.active',
  'grtoolkit.notifier.timer',
  'grtoolkit.notifier.only.active',
  'grtoolkit.notifier.only_tags'
];

function grtoolkitPrefsFlip()
{
  var bPref, xPref;

  bPref = document.getElementById('grtoolkit.context.active').checked;
  document.getElementById('grtoolkit.context.sep.a').disabled = !(bPref);
  document.getElementById('grtoolkit.context.sep.b').disabled = !(bPref);

  bPref = parseInt(document.getElementById('grtoolkit.link.option').value) == 2;
  document.getElementById('grtoolkit.link.background').disabled = !(bPref);

  bPref = document.getElementById('grtoolkit.notifier.active').checked;
  document.getElementById('grtoolkit.notifier.pause').disabled = !(bPref);
  document.getElementById('grtoolkit.notifier.pause.active').disabled = !(bPref);
  document.getElementById('grtoolkit.notifier.pause.label').disabled = !(bPref);
  document.getElementById('grtoolkit.notifier.timer').disabled = !(bPref);
  document.getElementById('grtoolkit.notifier.timer.label').disabled = !(bPref);
  document.getElementById('grtoolkit.notifier.only.active').disabled = !(bPref);

  xPref = document.getElementById('grtoolkit.notifier.pause.active').checked;
  document.getElementById('grtoolkit.notifier.pause').disabled = !(bPref && xPref);

  xPref = document.getElementById('grtoolkit.notifier.only.active').checked;
  document.getElementById('grtoolkit.notifier.only_tags').disabled = !(bPref && xPref);
  document.getElementById('grtoolkit.notifier.only_tags.label').disabled = !(bPref && xPref);
}

function grtoolkitPrefsInit()
{
  for( var i = 0; i < _elementIDs.length; i++ )
  {
    var elementID = _elementIDs[i];
    var element = document.getElementById(elementID);

    if (!element)
    {
      break;
    }
    else if (element.localName == 'checkbox')
    {
      try { element.checked = grPref.getBoolPref(elementID); }
      catch(e) { element.checked = false; }
    }
    else if (element.localName == 'radiogroup')
    {
      try { element.selectedItem = element.childNodes[grPref.getIntPref(elementID)]; }
      catch(e) { element.selectedItem = element.childNodes[0]; }
    }
    else if (element.localName == 'textbox')
    {
      if (element.getAttribute('preftype') == 'int')
      {
        try { element.value = grPref.getIntPref(elementID); }
        catch(e) { element.value = 300; }
      }
      else
      {
        try { element.value = grPref.getCharPref(elementID); }
        catch(e) { element.value = ''; }
      }
    }
  }

  grtoolkitPrefsFlip();
}

function grtoolkitPrefsSave()
{
  for( var i = 0; i < _elementIDs.length; i++ )
  {
    var elementID = _elementIDs[i];
    var element = document.getElementById(elementID);

    if (!element)
    {
      break;
    }
    else if (element.localName == 'checkbox')
    {
      grPref.setBoolPref(elementID, element.checked);
    }
    else if (element.localName == 'radiogroup')
    {
      grPref.setIntPref(elementID, parseInt(element.value));
    }
    else if (element.localName == 'textbox')
    {
      if (element.getAttribute('preftype') == 'int')
      {
        var bOkay = true;
        var cPref = '';
        var sPref = element.value.replace(/^[0]*/);
        var sWork = "0123456789";

        for (j = 0; j < sPref.length; j++)
        {
          if (sWork.indexOf(sPref.charAt(j)) == -1) bOkay = false;
          else cPref = cPref + sPref.charAt(j);
        }

        if (cPref.length == 0 ) cPref = '0';
        var iPref = parseInt(cPref);
        if (iPref < 30) iPref = 30;
        grPref.setIntPref(elementID, iPref);
      }
      else
      {
        grPref.setCharPref(elementID, element.value);
      }
    }
  }

  grtoolkitPrefsFlip();
}
