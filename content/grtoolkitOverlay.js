\include{comment.js}

// Initiate a new preference instance.
var grPref = Components.classes['@mozilla.org/preferences-service;1'].getService(Components.interfaces.nsIPrefBranch);

// Create the variables for string values.
//var gCitationsURL;
var grMyBlogsURL;
//var grPreviewURL;
//var grSaveItemURL;
var grSearchURL;
var grSubscribeURL;

// Create a variable for the stringbundle.
var grText;

// Create a variable for the timer process.
var grTimerID;

// Create a variable for HTTP requests.
var grXMLHttpRequest;

function grtoolkitInit()
{
  // Initialize stringbundle.
  grText = document.getElementById('grtoolkit-stringbundle');
  // Initialize string values.
  //grCitationsURL   = grText.getString('grtoolkit_url_citations');
  grMyBlogsURL     = grText.getString('grtoolkit_url_myblogs');
  //grPreviewURL     = grText.getString('grtoolkit_url_preview');
  //grSaveItemURL    = grText.getString('grtoolkit_url_saveitem');
  grSearchURL      = grText.getString('grtoolkit_url_search');
  grSubscribeURL   = grText.getString('grtoolkit_url_subscribe');

  var bPref;

  // Find transport layer preference.
  try      { bPref = grPref.getBoolPref('grtoolkit.link.secure'); }
  catch(e) { }

  // Make use of SSL if chosen.
  if (bPref) {
    //grCitationsURL = grCitationsURL.replace(/http/,'https');
    grMyBlogsURL   = grMyBlogsURL.replace(/http/,'https');
    //grPreviewURL   = grPreviewURL.replace(/http/,'https');
    //grSaveItemURL  = grSaveItemURL.replace(/http/,'https');
    grSearchURL    = grSearchURL.replace(/http/,'https');
    grSubscribeURL = grSubscribeURL.replace(/http/,'https');
  }

  // Create eventlistener to trigger when context menu is invoked.
  try
  {
    document.getElementById('contentAreaContextMenu').addEventListener('popupshowing',grtoolkitContext,false);
  }
  catch(e)
  {
    alert(e);
  }

  // Initial update of notifier icon.
  window.setTimeout(grtoolkitUpdateGet,1000);
}

function grtoolkitContext()
{
  var bPref;

  // Find context menu display preference.
  try      { bPref = grPref.getBoolPref('grtoolkit.context.active'); }
  catch(e) { }

  // Set hidden property of context menu and separators.
  document.getElementById('grtoolkit-context-menu').hidden = !(bPref);
  document.getElementById('grtoolkit-context-sep-a').hidden = !(bPref);
  document.getElementById('grtoolkit-context-sep-b').hidden = !(bPref);

  // If not displaying context menu, return.
  if (!bPref) return;

  // Separator A (above) display preference.
  try      { bPref = grPref.getBoolPref('grtoolkit.context.sep.a'); }
  catch(e) { bPref = false }
  document.getElementById('grtoolkit-context-sep-a').hidden = !(bPref);

  // Separator B (below) display preference.
  try      { bPref = grPref.getBoolPref('grtoolkit.context.sep.b'); }
  catch(e) { bPref = false }
  document.getElementById('grtoolkit-context-sep-b').hidden = !(bPref);

  // Should link items be hidden or shown?
  //document.getElementById('grtoolkit-context-saveitem-link').hidden = !(gContextMenu.onLink);
  //document.getElementById('grtoolkit-context-search-link').hidden = !(gContextMenu.onLink);
  document.getElementById('grtoolkit-context-subscribe-link').hidden = !(gContextMenu.onLink);

  // Should text search item be hidden or shown?
  //document.getElementById('grtoolkit-context-saveitem-text').hidden = !(gContextMenu.isTextSelected);
  document.getElementById('grtoolkit-context-search-text').hidden = !(gContextMenu.isTextSelected);
  if (gContextMenu.searchSelected)
  {
    document.getElementById('grtoolkit-context-search-text').setAttribute("label",grText.getFormattedString('grtoolkit_context_search_text_label',[gContextMenu.searchSelected(15)]));
  }
  else
  {
    document.getElementById('grtoolkit-context-search-text').setAttribute("label",grText.getFormattedString('grtoolkit_context_search_text_label',[getBrowserSelection(15)]));
  }
}

function grtoolkitOpenLink(url)
{
  var iPref;

  // Find link preference.
  try      { iPref = grPref.getIntPref('grtoolkit.link.option'); }
  catch(e) { }

  if (!iPref)
  {
    // Current Window
    getBrowser().loadURI(url);
  }
  else if (iPref == 1)
  {
    // New Window
    window.open(url);
  }
  else if (iPref == 2)
  {
    // New Tab
    var bPref;

    // Find context menu display preference.
    try      { bPref = grPref.getBoolPref('grtoolkit.link.background'); }
    catch(e) { }

    if (bPref)
    {
      // New Tab in Background
      getBrowser().addTab(url);
    }
    else
    {
      // New Tab in Foreground
      getBrowser().selectedTab = getBrowser().addTab(url);
    }
  }
}

function grtoolkitProcessClick(event)
{
  // Right-click event.
  if (event.button == 2)
  {
    window.openDialog('chrome://grtoolkit/content/grtoolkitPrefs.xul','PrefWindow','chrome,modal=yes,resizable=no','browser');
    return;
  }

  // Left-click event (also single click, like Mac).
  if (event.button == 0)
  {
    // Ctrl-click for Mac properties.  Works on PC too.
    if (event.ctrlKey)
    {
      window.openDialog('chrome://grtoolkit/content/grtoolkitPrefs.xul','PrefWindow','chrome,modal=yes,resizable=no','browser');
    }
    else
    {
      var stat = document.getElementById('grtoolkit-notifier-status').getAttribute('status');

      if (stat == '000')       // None unread.  Refresh.
      {
        grtoolkitUpdateGet();
        return;
      }
      if (stat == '00f')       // Disabled.  Show blogs.
      {
        grtoolkitReadItems();
        return;
      }
      if (stat == '0f0')       // Unread.  Display them.
      {
        grtoolkitReadItems();
        return;
      }
      if (stat == 'f00')       // Problems.  Show prefs.
      {
        window.openDialog('chrome://grtoolkit/content/grtoolkitPrefs.xul','PrefWindow','chrome,modal=yes,resizable=no','browser');
        return;
      }
    }
  }
}

function grtoolkitReadItems()
{
  var bPref;

  // See if we should pause notifier.
  try      { bPref = grPref.getBoolPref('grtoolkit.notifier.pause.active'); }
  catch(e) { }

  if (bPref)
  {
    var iPref;

    // Cancel previously submitted notifier job.
    window.clearTimeout(grTimerID);

    // Load the notifier pause timer.
    try      { iPref = grPref.getIntPref('grtoolkit.notifier.pause'); }
    catch(e) { iPref = 180; }

    // Re-submit with the appropriate poll timer.
    grTimerID = window.setTimeout(grtoolkitUpdateGet,iPref * 1000);

    // Set the icon status.
    document.getElementById('grtoolkit-notifier-status').setAttribute("status","000");
    document.getElementById('grtoolkit-notifier-status').setAttribute("tooltiptext",grText.getString('grtoolkit_msg_notifier_nounread'));
  }

  // Open up my blogs display.
  grtoolkitOpenLink(grMyBlogsURL);
}

function grtoolkitUpdateGet()
{
  var icon = document.getElementById('grtoolkit-notifier-status');
  var iPref, bPref;

  // Cancel previously submitted notifier job.
  window.clearTimeout(grTimerID);

  // Load the notifier poll timer.
  try      { iPref = grPref.getIntPref('grtoolkit.notifier.timer'); }
  catch(e) { iPref = 180; }

  // Re-submit with the appropriate poll timer.
  grTimerID = window.setTimeout(grtoolkitUpdateGet,iPref * 1000);

  // Find notifier update preference.
  try      { bPref = grPref.getBoolPref('grtoolkit.notifier.active'); }
  catch(e) { }

  if (!bPref)
  {
    icon.setAttribute("status","00f");
    icon.setAttribute("tooltiptext",grText.getString('grtoolkit_msg_notifier_disabled'));
    return;
  }
  else
  {
    icon.setAttribute("tooltiptext",grText.getString('grtoolkit_msg_notifier_fetching'));
  }

  var cPref;

  // Load the notifier email address.
  /*
  try
  {
    cPref = grPref.getCharPref('grtoolkit.notifier.email');
  }
  catch(e)
  {
    icon.setAttribute("status","f00");
    icon.setAttribute("tooltiptext",grText.getString('grtoolkit_msg_notifier_noemail'));
    return;
  }
  */
  grXMLHttpRequest = new XMLHttpRequest();
  grXMLHttpRequest.onload = grtoolkitUpdateSet;
  grXMLHttpRequest.open('GET','http://www.google.com/reader/api/0/unread-count?all=true&output=json&cl=grtoolkit');
  grXMLHttpRequest.setRequestHeader('User-Agent','GoogleReaderNotifier/Google Reader Toolkit for Mozilla/_VERSION_');
  grXMLHttpRequest.send(null);
}

function grtoolkitUpdateSet()
{

  var icon = document.getElementById('grtoolkit-notifier-status');
  var unreadData;
  var unreadCount = 0;
  
  try      { only = grPref.getBoolPref('grtoolkit.notifier.only.active'); }
  catch(e) { only = false; }

  try {
    tags = grPref.getCharPref('grtoolkit.notifier.only_tags').split(',');
  }
  catch(e) { only = false; }

  try {
    unreadData = eval("(" + grXMLHttpRequest.responseText + ")");

    for (var i = 0, pair; 
      pair = unreadData.unreadcounts[i]; 
      i++) {
        if (!only && 
            pair.id.match(/^user\/\d{20}\/state\/com\.google\/reading\-list$/)) {
          unreadCount = pair.count;
          break;
        }
        else if (only &&
                 (m=pair.id.match(/^user\/\d{20}\/label\/(.*)$/)) &&
                 tags.indexOf(m[1]) != -1) {
          unreadCount += pair.count;
        }
    }
  }
  catch (e) { unreadCount = null; }
  if (unreadCount==null)
  {
      icon.setAttribute("status","f00");
      icon.setAttribute("tooltiptext",grText.getString('grtoolkit_msg_notifier_baddata'));
  }
  else if (unreadCount==0) {
        icon.setAttribute("status","000");
        icon.setAttribute("tooltiptext",grText.getString('grtoolkit_msg_notifier_nounread'));
  }
  else {
        icon.setAttribute("status","0f0");
        icon.setAttribute("tooltiptext",grText.getFormattedString('grtoolkit_msg_notifier_unread',["" + unreadCount + (unreadCount == unreadData.max ? "+" : "")]));
  }
}

// Create event listener.
window.addEventListener('load',grtoolkitInit,false); 
