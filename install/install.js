const APP_DISPLAY_NAME        = "Google Reader Toolkit";
const APP_NAME                = "grtoolkit";
const APP_PACKAGE             = "/davidov/grtoolkit";
const APP_VERSION             = "_VERSION_";

const APP_JAR_FILE            = "grtoolkit.jar";
const APP_JAR_PATH            = "chrome/grtoolkit.jar";

var chromeFolder, chromeFlag;

initInstall(APP_NAME, APP_PACKAGE, APP_VERSION);

if (confirm("Install this into the application directory?  (Cancel will install into your profile directory)"))
{
  chromeFolder = getFolder("Chrome");
  chromeFlag = DELAYED_CHROME;
}
else
{
  chromeFolder = getFolder("Profile", "chrome");
  chromeFlag = PROFILE_CHROME;
}

var err = addFile(APP_PACKAGE, APP_VERSION, APP_JAR_PATH, chromeFolder, null)

if (err == SUCCESS)
{ 
  var jar = getFolder(chromeFolder, APP_JAR_FILE);
  registerChrome(CONTENT | chromeFlag, jar, "content/");
  registerChrome(LOCALE | chromeFlag, jar, "locale/en-US/");
//  registerChrome(LOCALE | chromeFlag, jar, "locale/es-ES/");
//  registerChrome(LOCALE | chromeFlag, jar, "locale/fr-FR/");
//  registerChrome(LOCALE | chromeFlag, jar, "locale/ja-JP/");
//  registerChrome(LOCALE | chromeFlag, jar, "locale/pt-BR/");
//  registerChrome(LOCALE | chromeFlag, jar, "locale/pt-PT/");
//  registerChrome(LOCALE | chromeFlag, jar, "locale/zh-TW/");	
  err = performInstall();

  if (err != 0 && err != 999)
  {
    alert("Install failed. Error code:" + err);
    cancelInstall(err);
  }
}
else
{ 
  alert("Failed to create " + APP_JAR_FILE + "\n"
    +"You probably don't have appropriate permissions \n"
    +"(write access to mozilla/chrome directory). \n"
    +"_____________________________\nError code:" + err);
  cancelInstall(err);
}
