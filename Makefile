content=$(wildcard content/*)
content_new=$(patsubst content/%,tmp/content/%,$(content))
locale_dirs=$(wildcard locale/*)
locale_dirs_new=$(patsubst locale/%,tmp/locale/%,$(locale_dirs))
locale_files=$(foreach dir,$(locale_dirs),$(wildcard $(dir)/*))
locale_files_new=$(patsubst locale/%,tmp/locale/%,$(locale_files))
install=$(wildcard install/*)
install_new=$(patsubst install/%,tmp/tree/%,$(install))
version=$(shell cat version)
xpi=Google_Reader_Toolkit_$(version).xpi
#разобраться с переменными

PP=gpp
PP_OPT=-U "" "" "\(" "," "\)" "\(" "\)" "\#" "\`" \
-M "\\" "" "{" "}{" "}" "{" "}" \
+c "\\\\" "\n" \
+n -x --warninglevel 2 -Itemplates --include script.gpp

all: $(xpi) 
	echo $(locale_files)

$(xpi): tmp/tree/chrome/grtoolkit.jar $(install_new)
	cd tmp/tree; zip -r ../../$@ *; cd ../..

$(filter-out %.png,$(content_new)): tmp/content $(filter-out %.png,$(content)) templates/script.gpp version
	$(PP) $(PP_OPT) $(subst tmp/content,content,$@) -o $@

$(filter %.png,$(content_new)): tmp/content $(filter %.png,$(content))
	cp $(subst tmp/content/,content/,$@) $@

$(locale_files_new): $(locale_files) $(locale_dirs_new) templates/script.gpp version
	$(PP) $(PP_OPT) $(subst tmp/locale,locale,$@) -o $@

$(install_new): tmp/tree $(install) templates/script.gpp version
	$(PP) $(PP_OPT) $(subst tmp/tree/,install/,$@) -o $@

tmp/tree/chrome tmp/content tmp/tree tmp/locale $(locale_dirs_new):
	mkdir -p $@

tmp/tree/chrome/grtoolkit.jar: $(content_new) $(locale_files_new) tmp/tree/chrome
	cd tmp; zip -r tree/chrome/grtoolkit.jar content locale; cd ..

clean:
	rm -Rf tmp *.xpi
